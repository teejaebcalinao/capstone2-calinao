//project the details of our selected course into that jumbotron
//to get the details of our selected course, we have to pass the id of the course into our
//fetch url.
/*

We have to get the value/parameter that is set into our url
pages/course.html?courseId=5fbb5549a3c1e73070d13caa

window.location.search -> returns the query string part of the URL

To get the actual courseId, we need to use URL search params to access specific parts of the query
*/
let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')

console.log(courseId)

//get the JWT in the localStorage
let token = localStorage.getItem('token')
let adminUser = localStorage.getItem('isAdmin')

console.log(token)//token

//populate the information of a course using fetch. But first, we have to gain access and save our elements into variables, so that we can project our course details into those elements.

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")
let enrolleeContainer = document.querySelector("#enrolleeContainer")

//get our course details
//fetch runs immediately
fetch(`https://shielded-castle-52683.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {


	/*
		Mini-Activity:

		Show the name of the course in the h2.
		Show the description of the course in the p
		Show the price of the course in the span

	*/

	// += - addition assignment operator
		//If you use addition assignment operator here, you are going to add into the innerHTML of an element, and thus if there was a previous content in the innerHTML of the element, it will add.
	// = assignment operator
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price



	if(adminUser === "true"){

		if(data.enrollees.length < 1){

			enrollContainer.innerHTML = "No Enrollees Available"

		} else {


			data.enrollees.forEach(enrollee => {

				fetch('https://shielded-castle-52683.herokuapp.com/api/users/').then(res=>res.json()).then(users=>{

					users.forEach(user => {


						if(enrollee.userId === user._id){
							enrolleeContainer.innerHTML += `

							<div class="card">
								<div class="card-body">
									<h5 class="card-title">${user.firstName} ${user.lastName}</h5>
									<p class="card-text text-center">${enrollee.enrolledOn}</p>
								</div>
							</div>

					`
						}

					})


				})


			})

	}

	} else {


		enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary" >Enroll</button>`

		//add a click event to our button
		document.querySelector("#enrollButton").addEventListener("click", ()=>{

			//no need to add e.preventDefault because the click event does not have a default behavior that refreshes the page unlike submit.

			if(token === null){
				console.log(token)
				window.location.replace("./register.html")
			}

			fetch('https://shielded-castle-52683.herokuapp.com/api/users/enroll', {

				method: 'POST',
				headers: {

					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`

				},
				body: JSON.stringify({

					courseId: courseId

				})

			})
			.then(res => res.json())
			.then(data => {

				if(data){
					alert("You have enrolled successfully.")
					window.location.replace("./courses.html")
				} else {

					alert("Enrollment Failed.")

				}

			})


		})

	}





})