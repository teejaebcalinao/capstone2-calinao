let navItems = document.querySelector("#navSession");
let userGreeting = document.querySelector("#userGreeting")

// localStorage => an object used to store information indefinitely locally in our devices
let userToken = localStorage.getItem("token");
let adminUser =localStorage.getItem("isAdmin")
/*
localStorage {
	token: "23o8ru32hrlh23kjlrhk2j3"
	isAdmin: false,
	getItem: function()
	setItem: function()
}
*/

// `` = backticks/julius back ticks/template string


//conditional rendering:
//dynamically add or delete an html element based on a condition
//Here we are adding our login and register links in the navbar ONLY if the user is not logged in.
//Otherwise, only the log out link will be shown.
if(!userToken) {
	//this is before we access and add 2 more li to the innerHTML
	console.log(navItems.innerHTML)
	navItems.innerHTML +=
	`
		<li class="nav-item ">
			<a href="./pages/login.html" class="nav-link"> Log in </a>
		</li>
		<li class="nav-item ">
			<a href="./pages/register.html" class="nav-link"> Register </a>
		</li>
	`
} else {

	if(adminUser === "true"){

		navItems.innerHTML += `

		<li class="nav-item ">
			<a href="./pages/logout.html" class="nav-link"> Log Out </a>
		</li>

		`

	} else {

		navItems.innerHTML += `

		<li class="nav-item ">
			<a href="./pages/profile.html" class="nav-link"> Profile </a>
		</li>
		<li class="nav-item ">
			<a href="./pages/logout.html" class="nav-link"> Log Out </a>
		</li>

		`

	}


}

