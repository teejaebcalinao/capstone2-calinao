/*get the login form and store in a variable*/
let loginForm = document.querySelector("#logInUser")

/*add a submit event to our login form, so that when the button is clicked to submit, we are able to run a function.*/
loginForm.addEventListener("submit", (e)=>{

	e.preventDefault()

	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value

/*	console.log(email)
	console.log(password)*/

	//check if our user was able to submit an email or password, if not, we'll show an alert, if yes, we'll proceed to the login fetch request.

	if(email == "" || password == ""){

		alert("Please input your email/password.")

	} else {

		/*
			Add a fetch request which will log in our user.

			-In the fetch request input the correct URL to use the proper route to our login.
			-Enter the correct parameters:
				method,
				headers,
				body
			-Our request body for login only needs 2 input for email and password
			-convert the server's response to json
			-after converting the json, use console.log() to check the data returned.

			This is similar to your registration.

			Take a screenshot of your code as activity-1


		*/

		fetch('https://shielded-castle-52683.herokuapp.com/api/users/login',{

			method: 'POST',
			headers: {

				'Content-Type': 'application/json'

			},
			body: JSON.stringify({

				email: email,
				password: password

			})

		})
		.then(res => res.json())
		.then(data => {

			//store our token in our localStorage.
			//localStorage is a storage for data in most modern browsers.
			//localStorage, however, can only store strings.

			//check if data/response is false or null, if there is no token or false is returned we will not save it in the localstorage

			if(data !== false && data.accessToken !== null){
				//if we are able to get a token, we'll then use it to get our user's details.
				//store the token in the localStorage:
				//setItem(<nameOftheKey>, <value>)
				localStorage.setItem('token', data.accessToken)

				fetch('https://shielded-castle-52683.herokuapp.com/api/users/details',{

					headers: {

						Authorization: `Bearer ${data.accessToken}`

					}


				})
				.then(res => res.json())
				.then(data => {

					localStorage.setItem('id', data._id)
					localStorage.setItem('isAdmin', data.isAdmin)
					localStorage.setItem('courses', JSON.stringify(data.enrollments))
					//console.log(localStorage)
					//redirect to our courses.html page
					window.location.replace('./courses.html')

				})


			}else {

				alert('Login Failed. Something went wrong.')

			}



		})


	}




})