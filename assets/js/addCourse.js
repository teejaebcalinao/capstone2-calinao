let formSubmit = document.querySelector("#createCourse")

formSubmit.addEventListener("submit", (e)=>{

	e.preventDefault()

	let courseName = document.querySelector("#courseName").value
	let description = document.querySelector("#courseDescription").value
	let price = document.querySelector("#coursePrice").value

	//get the JWT from our localStorage and save it in a variable called token
	let token = localStorage.getItem("token")

	/*
		Create the fetch request for adding a course.
			-add the correct URL and parameters
			-In the parameters:
				-add the correct method
				-add the correct headers(This request has a body, it will need information about what kind of data we are sending and a token.)
				-add the correct body
			-convert the response into json
			-console log the data converted as a json.

			Take a screenshot of your code as activity-3-code

	*/
	fetch('https://shielded-castle-52683.herokuapp.com/api/courses/',{

		method: 'POST',
		headers: {

			"Content-Type": "application/json",
			"Authorization": `Bearer ${token}` 
			//We are sending a token through HTTP. That is to say that we are "giving authorization to the bearer of this token."

		},
		body: JSON.stringify({

			name: courseName,
			description: description,
			price: price,

		})
	})
	.then(res => res.json())
	.then(data => {

		if(data){

			window.location.replace("./courses.html")

		} else {

			alert("Something went wrong. Course not added.")
		}

	})


})