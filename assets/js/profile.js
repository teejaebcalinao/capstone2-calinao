let profileContainer = document.querySelector("#profileContainer")
let token = localStorage.getItem("token")
let adminUser = localStorage.getItem("isAdmin")

fetch(`https://shielded-castle-52683.herokuapp.com/api/users/details`,{

	headers: {

		"Authorization": `Bearer ${token}`

	}

})
.then(res => res.json())
.then(data => {


	/*
		Mini-Activity:

		Show the name of the course in the h2.
		Show the description of the course in the p
		Show the price of the course in the span

	*/

	// += - addition assignment operator
		//If you use addition assignment operator here, you are going to add into the innerHTML of an element, and thus if there was a previous content in the innerHTML of the element, it will add.
	// = assignment operator


	if(adminUser === "true"){


			profileContainer.innerHTML = `<h1 class="text-center">No Profile Available</h1>`


	} else if (adminUser === "false") {

			let courseNames = []

			profileContainer.innerHTML = `

				<div class="col-md-12">
					<section class="jumbotron text-center my-5">		
						<h2 class="my-2">${data.firstName} ${data.lastName}</h2>
						<p> mobile number: ${data.mobileNo}</p>
						<div id="enrollmentsContainer">
						<h3>Courses</h3>
						</div>
					</section>
				</div>
			`
			let enrollmentCard = document.querySelector("#enrollmentsContainer")

			data.enrollments.map(enrollment => {

				fetch(`https://shielded-castle-52683.herokuapp.com/api/courses/${enrollment.courseId}`).then(res=>res.json()).then(data => {

					enrollmentCard.innerHTML += `


							<div class="card">
								<div class="card-body">
								    <h5 class="card-title">${data.name}</h5>
								    <h6 class="card-subtitle mb-2 text-muted">Enrolled On: ${Date(enrollment.enrolledOn).toLocaleString()}</h6>
								    <h6 class="card-subtitle mb-2 text-muted">Status:${enrollment.status}</h6>
								</div>
							</div>

						`

				})


			})



	}


			




})

